import React from "react";
import "./FavoritesPage.scss"


const FavoritesPage = ({ favorite }) => {
    return (
        <div>
            <h2 className="favorites-title">Faforites Page</h2>
            <ul className="favorites-list">
                {Array.isArray(favorite) && favorite.length > 0 ? (
                    favorite.map((product) => (
                        <li className="favorites-item" key={product.id}>
                            <div className="favorite-item-details">
                                <img
                                    className="favorite-item-image"
                                    src={product.url}
                                    alt={product.name}
                                />
                                <div className="favorite-item-info">
                                    <h3>{product.name}</h3>
                                    <p>Price: ${product.price.toFixed(2)}</p>
                                </div>
                            </div>
                        </li>
                    ))
                ) : (
                    <p>Your favorites list is empty.</p>
                )}
            </ul>
        </div>
    );
};

export default FavoritesPage;

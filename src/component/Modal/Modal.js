import React from 'react';
import './Modal.scss';

function Modal({ header, closeButton, text, actions, onClose }) {
  const handleClose = () => {
    if (onClose) {
      onClose();
    }
  };

  return (
    <div className="custom-modal-overlay" onClick={handleClose}>
      <div className="custom-modal" onClick={(e) => e.stopPropagation()}>
        {closeButton && (
          <span className="close-button" onClick={handleClose}>
            &times;
          </span>
        )}
        {header && <h2>{header}</h2>}
        <p>{text}</p>
        <div className="actions">
          {actions || (
            <>
              <button className='btn-cancel' onClick={handleClose}>CANCEL</button>
              <button className='btn-ok' onClick={handleClose}>OK</button>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default Modal;
